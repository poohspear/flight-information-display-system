            <footer class="footer">
                <div class="container">
                    <nav>
                        <ul class="footer-menu">
                            <li>
                                <a href="./../user-interface/" target="_blank" >
                                    <i class="nc-icon nc-tv-2"></i>
                                    &nbsp;&nbsp;Arrival Table
                                </a>
                            </li>
                            <li>
                                <a href="./../user-interface/departure.php" target="_blank">
                                    <i class="nc-icon nc-tv-2"></i> 
                                    &nbsp;&nbsp;Deparature Table
                                </a>
                            </li>
                        </ul>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            , made with love in sunny Myanmar.
                        </p>
                    </nav>
                </div>
            </footer>