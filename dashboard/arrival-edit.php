<?php
ob_start();
session_start();
include("./require.php");

if(isset($_POST['submit'])){
    $sql = "
        UPDATE 
          tbl_arrival 
          SET 
              time = '".$_POST['time']."', 
              airline = '".$_POST['airline']."', 
              flight = '". $_POST['flight'] ."', 
              belt = '".$_POST['belt']."', 
              status = '".$_POST['status']."', 
              landed_time = '".$_POST['landed_time']."' 
          WHERE 
              id = '".$_POST['id']."'; ";
    $update = mysqli_query($conn, $sql);
    if(!$update){
        echo mysqli_error($conn);
        die();
    }
    header("Location: arrival-list.php?update=success");
}else{
    $update = false;
}

$result_airline = mysqli_query($conn, " SELECT * FROM tbl_airline ");
$result_status = mysqli_query($conn, " SELECT * FROM tbl_status WHERE type = 'Arrival' OR type = 'Both' ");
$id = mysqli_real_escape_string($conn, $_GET['id']);
$sql_form = " 
    SELECT 

        tbl_status.id, 
        tbl_status.eng, 
        tbl_status.mm, 
        tbl_status.color, 
        tbl_status.type,

        tbl_arrival.id,
        tbl_arrival.time,
        tbl_arrival.airline,
        tbl_arrival.flight,
        tbl_arrival.belt,
        tbl_arrival.status,
        tbl_arrival.landed_time

     FROM 
        tbl_arrival 
        LEFT JOIN
        tbl_status ON tbl_arrival.status = tbl_status.id
      WHERE 
        tbl_arrival.id = '{$id}' ";
$result_form = mysqli_query($conn, $sql_form);
$row = mysqli_fetch_array($result_form);

$status_json = file_get_contents("status.json");
$status_rows = json_decode($status_json);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Dashboard</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="./../bower_components/components-font-awesome/css/all.min.css" />

    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <style type="text/css">
        td img {
            width: 30px !important;
            padding-right: 10px !important;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="./../assets/img/sidebar-5.jpg">
            <?php include("sidebar.php");?>
        </div>
        <div class="main-panel">
            <?php include("nav.php"); ?>
            <div class="content">
                <div class="container">
                    <div class="card" style="margin-top: 30px;">
                        <div class="card-header ">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6 mr-auto ml-auto">
                                        <h4 class="card-title">Departure Edit</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="arrival-edit.php?id=<?=$row['id'];?>" method="post">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <h4 class="title">Time</h4>
                                            <div class="form-group">
                                                <input name="id" value="<?=$row['id'];?>" type="hidden" />
                                                <input name="time" value="<?=$row['time'];?>" type="text" class="form-control timepicker" placeholder="Time Picker Here" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 mr-auto ml-auto">
                                        <h4 class="title">Airline</h4>
                                        <div class="form-group">
                                          <div class="form-check form-check-radio">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="radio" name="airline" id="airline" value='<?=$row['airline']?>' checked />
                                              <span class="form-check-sign"></span>
                                              <?=str_replace("./data/","../data/",$row['airline']);?> (Default)
                                            </label>
                                          </div>
                                          <?php
                                          $no=1;
                                          while ($row_airline = mysqli_fetch_array($result_airline)):
                                          ?>
                                            <div class="form-check form-check-radio">
                                              <label class="form-check-label">
                                                <input 
                                                  class="form-check-input" 
                                                  type="radio" 
                                                  name="airline" 
                                                  id="airline"
                                                  value='<img src=\"./data/<?=$row_airline['id'];?>.png\" /><?=$row_airline['airline'];?>'>
                                                <span class="form-check-sign"></span>
                                                <img src="../data/<?=$row_airline['id'];?>.png" /> <?=$row_airline['airline'];?>
                                              </label>
                                            </div>
                                          <?php endwhile;?>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <h4 class="title">Flight</h4>
                                            <div class="form-group">
                                                
                                                <textarea 
                                                    name="flight" 
                                                    class="form-control" 
                                                    rows="2" style="min-height: 4em;"
                                                    ><?=$row['flight']?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <h4 class="title">Belt</h4>
                                            <div class="form-group">
                                                <input name="belt" value="<?=$row['belt'];?>" type="text" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                            <div class="row">
                            <div class="col-md-6 mr-auto ml-auto">
                              <h4 class="title">Status</h4>
                              <div class="form-group">
                              <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="status" id="status" value='<?=$row['status']?>' checked />
                                <span class="form-check-sign"></span>
                                <span class="<?=$row['color'];?>">
                                  <?=$row['eng'];?>
                                  <?=$row['mm'];?>
                                </span>

                                (Default)
                                </label>
                              </div>
                              <?php
                              $no=1;
                              while ($row_status = mysqli_fetch_array($result_status)):
                              ?>
                                <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                  <input 
                                  class="form-check-input" 
                                  type="radio" 
                                  name="status" 
                                  id="status"
                                  value='<?=$row_status['id'];?>'>
                                  <span class="form-check-sign"></span>
                                  <span class="<?=$row_status['color'];?>">
                                  <?=$row_status['eng'];?>
                                  <?=$row_status['mm'];?>
                                  </span>
                                </label>
                                </div>
                              <?php endwhile;?>
                              </div>
                            </div>
                            </div>
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <h4 class="title">Landed Time (If choosen is landed)</h4>
                                            <div class="form-group">
                                                <input type="text" name="landed_time" value="<?=$row['landed_time'];?>" class="form-control timepicker" placeholder="Time Picker Here" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mr-auto ml-auto">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <a href="./" class="btn btn-outline btn-primary btn-block ">Back</a>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <input type="submit" name="submit" class="btn btn-primary btn-block " value="Save"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <? include("footer.php") ?>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Bootstrap Select  -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!-- Form Costum -->
<script>
    $('.timepicker').datetimepicker({
        //          format: 'H:mm',    // use this format if you want the 24hours timepicker
        format: 'h:mm A', //use this format if you want the 12hours timpiecker with AM/PM toggle
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
</script>
</html>