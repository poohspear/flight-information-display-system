<?php
ob_start();
session_start();
include("./require.php");
$sql = "

  SELECT
    tbl_status.id,
    tbl_status.eng,
    tbl_status.mm,
    tbl_status.color,
    tbl_status.type,

    tbl_departure.id,
    tbl_departure.time,
    tbl_departure.airline,
    tbl_departure.flight,
    tbl_departure.checkin,
    tbl_departure.status
   FROM
    tbl_departure
    LEFT JOIN
    tbl_status ON tbl_departure.status = tbl_status.id";
$result_departure = mysqli_query($conn, $sql);
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Dashboard</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="./../bower_components/components-font-awesome/css/all.min.css" />

  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/css/demo.css" rel="stylesheet" />
  <style type="text/css">
    td img {
      width: 30px !important;
      padding-right: 10px !important;
    }
  </style>
</head>
<body>
  <div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="./../assets/img/sidebar-5.jpg">
      <?php include("sidebar.php");?>
    </div>
    <div class="main-panel">
      <?php include("nav.php"); ?>
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="card strpied-tabled-with-hover" style="margin-top:30px;">
                <div class="card-header" id="departure">
                  <h4 class="card-title">
                    Departure Status
                  </h4>
                  <p class="text-right">
                    <a href="departure-add.php" class="btn btn-outline btn-primary"> + Add</a>
                    <a href="departure-download.php" class="btn btn-outline btn-primary">Download</a>
                  </p>
                </div>
                <div class="card-body table-full-width table-responsive">
                  <table class="table table-hover table-striped">
                    <thead>
                      <th>No.</th>
                      <th>Time</th>
                      <th>Airline</th>
                      <th>Flight</th>
                      <th>Check-In</th>
                      <th>Status</th>
                      <th class="text-right">Action</th>
                    </thead>
                    <tbody>
                      <?php
                        $no=1;
                        while ($row = mysqli_fetch_array($result_departure)) {
                          ?>
                            <tr>
                              <td>No.(<?=$no;?>)</td>
                              <td><?=$row['time']?></td>
                              <td><?=str_replace("./data/","../data/",$row['airline']);?></td>
                              <td><?=nl2br($row['flight'])?></td>
                              <td><?=$row['checkin']?></td>
                              <td>
                                <span class="<?=$row['color'];?>">
                                  <?=$row['eng'];?>
                                  <?=$row['mm'];?>
                                </span><br>
                              </td>
                              <td class="td-actions text-right" style="">
                                <a
                                  rel="tooltip"
                                  title=""
                                  class="btn btn-link btn-warning table-action edit"
                                  href="departure-edit.php?id=<?=$row['id']?>"
                                  data-original-title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a
                                  rel="tooltip"
                                  title=""
                                  class="btn btn-link btn-danger table-action remove"
                                  data-toggle="modal" data-target="#myModal<?=$row['id']?>"
                                  href="#"
                                  data-original-title="Remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <!-- Mini Modal -->
                                <div class="modal fade modal-mini modal-primary" id="myModal<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header justify-content-center">
                                        <div class="modal-profile">
                                          <i class="nc-icon nc-bulb-63"></i>
                                        </div>
                                      </div>
                                      <div class="modal-body text-center">
                                        <p>Are you sure want to delete?</p>
                                      </div>
                                      <div class="modal-footer">
                                        <a href="departure-delete.php?id=<?=$row['id']?>" class="btn btn-link btn-simple btn-danger">Delete</a>
                                        <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!--  End Modal -->
                              </td>
                            </tr>
                          <?php
                          $no++;
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <? include("footer.php") ?>
    </div>
  </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<?php if(isset($_GET['delete'])): ?>
<script type="text/javascript">
  $.notify({
    icon: "nc-icon nc-app",
    title: "Delete Success",
    message: "Your record have been deleted successfully."
  },{
    // settings
    type: 'danger'
  });
</script>
<?php endif;?>

<?php if(isset($_GET['insert'])): ?>
<script type="text/javascript">
  $.notify({
    icon: "nc-icon nc-app",
    title: "Add Success",
    message: "Your record have been added successfully."
  },{
    // settings
    type: 'success'
  });
</script>
<?php endif;?>

<?php if(isset($_GET['update'])): ?>
<script type="text/javascript">
  $.notify({
  icon: "nc-icon nc-app",
  title: "Update Success",
  message: "Your record have been updated successfully."
  },{
  // settings
  type: 'success'
  });
</script>
<?php endif;?>
</html>
