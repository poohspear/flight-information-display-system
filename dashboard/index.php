<?php
ob_start();
session_start();
include("./require.php");

$result_arrival = mysqli_query($conn, " SELECT * FROM tbl_arrival ");
$num_rows_arrival = mysqli_num_rows($result_arrival);

$result_departure = mysqli_query($conn, " SELECT * FROM tbl_departure ");
$num_rows_departure = mysqli_num_rows($result_departure);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Dashboard</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="./../bower_components/components-font-awesome/css/all.min.css" />

    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <style type="text/css">
        td img {
            width: 30px !important;
            padding-right: 10px !important;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="../assets/img/sidebar-5.jpg">
            <?php include("sidebar.php");?>
        </div>
        <div class="main-panel">
            <?php include("nav.php"); ?>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card card-stats">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="fa fa-plane-departure text-info"></i>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="numbers">
                                                <p class="card-category">Departure</p>
                                                <h4 class="card-title"><?=$num_rows_departure;?></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="fa fa-plane-arrival text-info"></i>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="numbers">
                                                <p class="card-category">Arrival</p>
                                                <h4 class="card-title"><?=$num_rows_arrival;?></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <? include("footer.php") ?>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<?php
/*
<script type="text/javascript">
    $.notify({
        icon: "nc-icon nc-app",
        message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."
    },{
        // settings
        type: 'success'
    });
</script>
*/
?>
</html>