    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg ">
        <div class="container">
            <div class="navbar-wrapper">
                <a class="navbar-brand" href="./">
                    <i class="fa fa-plane"></i>
                    Nyaung U Airport
                </a>
            </div>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar burger-lines"></span>
                <span class="navbar-toggler-bar burger-lines"></span>
                <span class="navbar-toggler-bar burger-lines"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="nc-icon nc-tv-2"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="./../user-interface/arrival.php" target="_blank" style="line-height: 1.3em;">
                                <i class="nc-icon nc-tv-2"></i>
                                <span style="font-size: .7em;">Arrival Table</span>
                            </a>
                            <a class="dropdown-item" href="./../user-interface/departure.php" target="_blank" style="line-height: 1.3em;">
                                <i class="nc-icon nc-tv-2"></i>
                                <span style="font-size: .7em;">Deparature Table</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
