            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="./" class="simple-text ml-3" >
                        Flight Dashboard
                    </a>
                    <div class="clearfix"></div>
                </div>
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#departure_collapse">
                            <i class="fas fa-plane-departure"></i>
                            <p>
                                Departure
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="departure_collapse">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="./departure-list.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-single-copy-04"></i></span>
                                        <span class="sidebar-normal">Departure</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./departure-add.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-simple-add"></i></span>
                                        <span class="sidebar-normal">Departure Add</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./departure-download.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-cloud-download-93"></i></span>
                                        <span class="sidebar-normal">Download</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#arrival_collapse">
                            <i class="fa fa-plane-arrival"></i>
                            <p>
                                Arrival
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="arrival_collapse">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="./arrival-list.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-single-copy-04"></i></span>
                                        <span class="sidebar-normal">Arrival</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./arrival-add.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-simple-add"></i></span>
                                        <span class="sidebar-normal">Arrival Add</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./arrival-download.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-cloud-download-93"></i></span>
                                        <span class="sidebar-normal">Download</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#airline_collapse">
                            <i class="fa fa-plane"></i>
                            <p>
                                Airline
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="airline_collapse">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="./airline-list.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-single-copy-04"></i></span>
                                        <span class="sidebar-normal">Airline</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./airline-add.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-simple-add"></i></span>
                                        <span class="sidebar-normal">Airline Add</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#status_collapse">
                            <i class="fa fa-clock"></i>
                            <p>
                                Status
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="status_collapse">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="./status-list.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-single-copy-04"></i></span>
                                        <span class="sidebar-normal">Status</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="./status-add.html">
                                        <span class="sidebar-mini"><i class="nc-icon nc-simple-add"></i></span>
                                        <span class="sidebar-normal">Status Add</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#history_collapse">
                            <i class="fa fa-file"></i>
                            <p>
                                History
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="history_collapse">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="./status-list.php">
                                        <span class="sidebar-mini"><i class="nc-icon nc-single-copy-04"></i></span>
                                        <span class="sidebar-normal">View History</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li-->
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">
                            <i class="fa fa-user"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
