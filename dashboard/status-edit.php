<?php
ob_start();
session_start();
include("./require.php");

if(isset($_POST['id'])){
  $sql = "
    UPDATE 
      tbl_status 
      SET 

        eng = '".$_POST['eng']."' , 
        mm = '".$_POST['mm']."' , 
        color = '".$_POST['color']."' , 
        type = '".$_POST['type']."'

      WHERE 
        id = '".$_POST['id']."'; ";

  $update = mysqli_query($conn, $sql);
  if(!$update){
    echo mysqli_error($conn);
    die();
  }else{
    header("Location: status-list.php?update=success");
  }
}else{
  $update = false;
}

$id = mysqli_real_escape_string($conn, $_GET['id']);
$result = mysqli_query($conn, " SELECT * FROM tbl_status WHERE id = '{$id}' ");
$row = mysqli_fetch_array($result);
extract($row)
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Dashboard</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="./../bower_components/components-font-awesome/css/all.min.css" />

  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/css/demo.css" rel="stylesheet" />
  <style type="text/css">
    td img {
      width: 30px !important;
      padding-right: 10px !important;
    }
  </style>
</head>
<body>
  <div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="./../assets/img/sidebar-5.jpg">
      <?php include("sidebar.php");?>
    </div>
    <div class="main-panel">
      <?php include("nav.php"); ?>
      <div class="content">

        <div class="container">
          <div class="card" style="margin-top: 30px;">
            <div class="card-header ">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6 mr-auto ml-auto">
                    <h4 class="card-title">Airline Edit</h4>
                  </div>
                </div>
              </div>
            </div>
            <form action="status-edit.php?id=<?=$id?>" method="post" class="card-body" enctype="multipart/form-data">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6 mr-auto ml-auto">
                    <h4 class="title">English</h4>
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="id" value="<?=$id?>" />
                      <input 
                        type="text" 
                        class="form-control" 
                        name="eng" 
                        value="<?=$eng?>" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 mr-auto ml-auto">
                    <h4 class="title">Myanmar</h4>
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="id" value="<?=$id?>" />
                      <input 
                        type="text" 
                        class="form-control" 
                        name="mm" 
                        value="<?=$mm?>" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 mr-auto ml-auto">
                    <h4 class="title">Type</h4>
                    <div class="form-group">
                      <div class="form-check form-check-radio">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="type" id="type" value="<?=$type?>" checked>
                              <span class="form-check-sign"></span>
                              Default <?=$type?>
                          </label>
                      </div>
                      <div class="form-check form-check-radio">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="type" id="type" value="Departure">
                              <span class="form-check-sign"></span>
                              Departure
                          </label>
                      </div>
                      <div class="form-check form-check-radio">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="type" id="type" value="Arrival">
                              <span class="form-check-sign"></span>
                              Arrival
                          </label>
                      </div>
                      <div class="form-check form-check-radio">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="type" id="type" value="Both">
                              <span class="form-check-sign"></span>
                              Both
                          </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 mr-auto ml-auto">
                    <h4 class="title">Color</h4>
                    <div class="form-group">
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="<?=$color?>" checked>
                          <span class="form-check-sign"></span>
                          <span class="<?=$color?>">Default</span>
                        </label>
                      </div>
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="text-primary">
                          <span class="form-check-sign"></span>
                          <span class="text-primary">Primary</span>
                        </label>
                      </div>
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="text-success">
                          <span class="form-check-sign"></span>
                          <span class="text-success">Success</span>
                        </label>
                      </div>
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="text-info">
                          <span class="form-check-sign"></span>
                          <span class="text-info">Info</span>
                        </label>
                      </div>
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="text-warning">
                          <span class="form-check-sign"></span>
                          <span class="text-warning">Warning</span>
                        </label>
                      </div>
                      <div class="form-check form-check-radio">
                        <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="color" id="color" value="text-danger">
                          <span class="form-check-sign"></span>
                          <span class="text-danger">Danger</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-6 mr-auto ml-auto">
                      <div class="row">
                        <div class="form-group col-md-6">
                          <a href="./" class="btn btn-outline btn-primary btn-block ">Back</a>
                        </div>
                        <div class="form-group col-md-6">
                          <input type="submit" name="submit" class="btn btn-primary btn-block " value="Save"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
          </div>
          <!-- end card -->
        </div>
      </div>
      <? include("footer.php") ?>
    </div>
  </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Bootstrap Select  -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!-- Form Costum -->
<script>
  $('.timepicker').datetimepicker({
    //          format: 'H:mm',    // use this format if you want the 24hours timepicker
    format: 'h:mm A', //use this format if you want the 12hours timpiecker with AM/PM toggle
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down",
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-screenshot',
      clear: 'fa fa-trash',
      close: 'fa fa-remove'
    }
  });
</script>
</html>