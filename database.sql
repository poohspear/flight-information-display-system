-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 03, 2019 at 05:56 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flight_schedule`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_airline`
--

CREATE TABLE `tbl_airline` (
  `id` int(10) NOT NULL,
  `airline` varchar(100) COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Dumping data for table `tbl_airline`
--

INSERT INTO `tbl_airline` (`id`, `airline`) VALUES
(1, 'Air KBZs'),
(2, 'Golden Myanmar Airline'),
(3, 'Mann Yadanarpon Airline'),
(4, 'Myanmar National Airline');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_arrival`
--

CREATE TABLE `tbl_arrival` (
  `id` int(11) NOT NULL,
  `time` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `airline` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `flight` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `belt` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `status` varchar(500) COLLATE utf8_german2_ci NOT NULL,
  `landed_time` varchar(100) COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departure`
--

CREATE TABLE `tbl_departure` (
  `id` int(11) NOT NULL,
  `time` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `airline` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `flight` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `checkin` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `status` varchar(500) COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id` int(10) NOT NULL,
  `eng` varchar(500) COLLATE utf8_german2_ci NOT NULL,
  `mm` varchar(500) COLLATE utf8_german2_ci NOT NULL,
  `color` varchar(100) COLLATE utf8_german2_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id`, `eng`, `mm`, `color`, `type`) VALUES
(8, 'Check In', 'ထွက်ခွာသတင်းပို့ရန်', 'text-primary', 'Departure'),
(9, 'Gate Open', 'ဂိတ်ဖွင့်ပြီး', 'text-primary', 'Departure'),
(10, 'Gate Close', 'ဂိတ်ပိတ်ပြီး', 'text-primary', 'Departure'),
(11, 'Confirmed', 'အတည်ပြုပြီး', 'text-primary', 'Arrival'),
(12, 'Cancelled', 'ခရီးစဉ်ဖျက်သိမ်းခြင်း', 'text-warning', 'Both'),
(13, 'Delayed', 'နောက်ကျမည်', 'text-warning', 'Both'),
(14, 'On Time', 'အချိန်မှန်', 'text-primary', 'Arrival');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_airline`
--
ALTER TABLE `tbl_airline`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_arrival`
--
ALTER TABLE `tbl_arrival`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_departure`
--
ALTER TABLE `tbl_departure`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_airline`
--
ALTER TABLE `tbl_airline`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_arrival`
--
ALTER TABLE `tbl_arrival`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_departure`
--
ALTER TABLE `tbl_departure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
