<?php
session_start();
ob_start();
include("./../dbcon.php");

$date_now = date("m/d/Y");
$date_now = strtotime($date_now);
$date=date_create("02/01/2020");
$date_convert = date_format($date,"m/d/Y");
$date_convert = strtotime($date_convert);

if ($date_now > $date_convert) {
  die("<tr><th colspan='5'><p class='text-center'>Error...</p></th></tr>");
}

$sql = "

    SELECT
      tbl_status.id,
      tbl_status.eng,
      tbl_status.mm,
      tbl_status.color,
      tbl_status.type,

      tbl_departure.id,
      tbl_departure.time,
      tbl_departure.airline,
      tbl_departure.flight,
      tbl_departure.checkin,
      tbl_departure.status
     FROM
      tbl_departure
      LEFT JOIN
      tbl_status ON tbl_departure.status = tbl_status.id
     ORDER BY
       STR_TO_DATE(tbl_departure.time,'%h:%i %p') ASC
    ";

$uniq = $_GET['q'];

// Calculate Total Page
$result_table = mysqli_query($conn, " $sql ");
$per_page = 9;
$total = mysqli_num_rows($result_table);
$total_pages = ceil($total / $per_page);
// Calculate Page Number
$page_number = isset($_SESSION[$uniq.'page_number']) ? ($_SESSION[$uniq.'page_number']+1) : 1;
$page_number = $page_number <= $total_pages ? $page_number : 1;
$_SESSION[$uniq.'page_number'] = $page_number;
// Export table
$offset = ($page_number * $per_page) - $per_page;
$limit = $per_page;
$result_table = mysqli_query($conn, " $sql LIMIT {$offset}, {$limit} ;");
// Calculate empty rows
$num_row = mysqli_num_rows($result_table);
$empty_row = 9 - $num_row;

while ($row = mysqli_fetch_array($result_table)) {
    $airline =  str_replace("./data/","../data/",$row['airline']);
    $dom = new DOMDocument;
    $dom->loadHTML($airline);
    $airline_img = $dom->getElementsByTagName('img');
    $src = $airline_img[0]->getAttribute('src');
    $airline_img[0]->parentNode->removeChild($airline_img[0]);
    $airline_name = $dom->saveHTML();
    ?>
    <tr>
      <td><?=$row['time']?></td>
      <td><img src="<?=$src?>" alt="" /></td>
      <td><?=$airline_name?></td>
      <td><?=nl2br($row['flight'])?></td>
      <td><?=$row['checkin']?></td>
      <td class="status">
          <span class="eng <?=$row['color'];?>">
            <?=$row['eng'];?>
          </span>
          <span class="mm <?=$row['color'];?>">
            <?=$row['mm'];?>
          </span>
      </td>
    </tr>
    <?php
}
$i = 1;
while ($i <= $empty_row){
    $i++;
    ?>
    <tr>
      <td></td><td></td><td></td><td></td><td></td><td></td>
    </tr>
    <?php
}
?>
<tr style="height:2em;">
  <td colspan="5">
      <?=date('j M Y');?>
  </td>
  <td class="text-right">
      <?=date("h:i:s A");?>
  </td>
</tr>
