<?php
session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./../assets/css/bootstrap.min.css">
    <title>Departure</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body class="bg-dark">
      <table class="table table-dark">
        <thead>
          <tr>
            <th scope="col">Time</th>
            <th scope="col" width="40px"></th>
            <th scope="col">Airline</th>
            <th scope="col">Flight</th>
            <th scope="col">Check-In</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
            <th colspan="5">
              <p class="text-center">
                Please click anywhere to activate flight schedule table.
              </p>
            </th>
        </tbody>
      </table>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="./../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="./../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        // Page reload after 10 seconds
        $(document).ready(function(){
            $("tbody").load("departure-table.php?q=<?=uniqid()?>");
            $("body").click(function(){
              $("tbody").load("departure-table.php?q=<?=uniqid()?>");
            });
        });
        window.setInterval(function(){
          $("tbody").load("departure-table.php?q=<?=uniqid()?>");
          $("#progress-bar-holder").html('<div class="loading-progress-bar" style="width:100%;border: 1px solid white;height: 0px;"></div>');
          console.log("table load");
        },10000);


        // Document fullscreen
        var elem = document.documentElement;
        function openFullscreen() {
          if (elem.requestFullscreen) {
            elem.requestFullscreen();
          } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
          } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
          } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
          }
        }

        function closeFullscreen() {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
          } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
          }
        }

        $("body, html").click(function(){
          openFullscreen();
        });

    </script>
  </body>
</html>
